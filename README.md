# IkosianCRM Client

Simple client which connects to IkosianCRM and transcodes contacts retrieved via the API to vCards to be used in Outlook etc.

## Setup

A file `config.json` needs to hold the information about the IkosianCRM API (incl. credentials and selected account managers) and the version of the vCard to be created (recommended is version `3.0`). An example for this file is given in `config.templ.json`.

After setting up the config, dependencies must be installed via `npm install`.

Then, the client can be run via `node index.js`. vCards are created in the folder `/vCards`.

## To do

Following attributes from IkosianCRM are not transcoded into the vCard at the moment:

* id (number): ID of contact for future reference
