var yargs = require('yargs').argv;
var accountmanagers = yargs.am;

var settings = require('./config');

require('./lib/String');

console.log('Retrieving and parsing data from TriadCRM for ' + accountmanagers);

// libraries

var _ = require('underscore');
var vCardJS = require('vcards-js');
var Download = require('download');

var spinner = require('simple-spinner');
spinner.start();

/////////////
// Runtime //
/////////////

new Download({
    mode: '755'
  })
  .get(settings.IkosianCRM.url + accountmanagers)
  .dest('./temp')
  .rename('dl.json')
  .run(function(err, files) {

    if (err) {
      console.log(err);
      spinner.stop();
      return;
    }

    var peoples = require('./temp/dl');
    var people = _.keys(peoples);
    for (var i = 0; i < people.length; i++) {
      var person = peoples[people[i]];

      var vCard = vCardJS();

      if (person.sal) vCard.namePrefix = person.sal.hexDecode().htmlDecode();
      if (person.n) vCard.firstName = person.n.hexDecode().htmlDecode();
      if (person.sn) vCard.lastName = person.sn.hexDecode().htmlDecode();

      if (person.div) vCard.title = person.div.hexDecode().htmlDecode();
      if (person.co) vCard.organization = person.co.hexDecode().htmlDecode();

      if (person.psrt) vCard.homeAddress.street = person.pstr.hexDecode().htmlDecode();
      if (person.pzip) vCard.homeAddress.city = person.pzip.hexDecode().htmlDecode();

      if (person.cstr) vCard.workAddress.street = person.cstr.hexDecode().htmlDecode();
      if (person.czip) vCard.workAddress.city = person.czip.hexDecode().htmlDecode();

      if (person['pho-co']) vCard.workPhone = person['pho-co'].hexDecode().htmlDecode();
      if (person['pho-pr']) vCard.homePhone = person['pho-pr'].hexDecode().htmlDecode();
      if (person['mob-co']) vCard.cellPhone = person['mob-co'].hexDecode().htmlDecode();
      if (person['mob-co']) vCard.pagerPhone = person['mob-pr'].hexDecode().htmlDecode();
      if (person['fax-co']) vCard.workFax = person['fax-co'].hexDecode().htmlDecode();
      if (person['fax-pr']) vCard.homeFax = person['fax-pr'].hexDecode().htmlDecode();

      if (person.email) vCard.workEmail = person.email;
      if (person['email-pr']) vCard.email = person['email-pr'];

      var unixts = parseInt(person.bday);
      if (unixts) {
        vCard.birthday = new Date(unixts * 1000);
      }

      if (person.note) vCard.note = person.note.hexDecode().htmlDecode();

      vCard.version = settings.vCard.version;

      //save to file
      vCard.saveToFile('./vCards/' + person.n + '-' + person.sn + '.vcf');

    }

    spinner.stop();

  });
