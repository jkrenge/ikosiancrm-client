var Entities = require("entities");

String.prototype.hexDecode = function() {
  var r = /\\u([\d\w]{4})/gi;
  var x = this.replace(r, function (match, grp) {
      return String.fromCharCode(parseInt(grp, 16)); } );
  return unescape(x);
};

String.prototype.htmlDecode = function () {
  return Entities.decode(this);
};
